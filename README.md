# MULTI EMG DEVICE #
v. 1.0 - 25 June 2015

This software is used to acquire multiple devices running at different frequencies.
It can record Thalmic Myo, Cometa Waves, data coming from socket.
You can define what to record and all possible options with the xml config file.
It's used to expand the capabilities of the NinaPro software, even if with some additional coding it can be used standalone.
It's currently used by the Hes-so University in Switzerland.

-----------------------------------------------------------------------------

This software interfaces with the NinaPro software. 
It takes data from a device and uses TCP sockets to send these data to NinaPro. NinaPro will then use tddTrigno-like object to run the exercises. 
It can also log these data into files.

Devices can also be dummy, configuring accordingly the xml configuration file.

The contributors to this project are: Stefano Pizzolato, Monica Reggiani, Luca Tagliapietra

##TODO##

* if accelerometer is not used NinaPro shouldn't record it. NinaPro waits for a accelerometer data, which is exploited using a dummy myo device with 0 emg channels and 3 fake accelerometer channels.
* review and comment
* add QUIT command in the communication sockets and properly close sockets


##SETUP##

A customized version of NinaPro is required to run multiEmgDevice. Stefano Pizzolato has it.

C++11 compiler is required.

You need the following libraries to make MultiEmgDevice work.

* Boost libraries
* Custom Wave API
* Myo API
* CodeSynthesis XSD 4.0 with Xerces

CMake is required to build this project

Environment variables must be set for CMake if libraries are installed in non-default paths

BOOST_ROOT

MYO_ROOT

WAVEAPI_ROOT

XERCES_ROOT

XSD_ROOT (which might be the same as XERCES_ROOT)

NinaPro requires NINAPRO_MOVIES to be set