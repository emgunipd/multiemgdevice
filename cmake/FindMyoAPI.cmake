# Find Myo API library
# Author: Luca Tagliapietra   <luca88.22@gmail.com>

find_path(MyoAPI_INCLUDE_DIR myo/myo.hpp
          PATHS ENV MYO_ROOT
          PATH_SUFFIXES include)

if(NOT MyoAPI_FIND_QUIETLY)
    message( STATUS "Myo include path: ${MyoAPI_INCLUDE_DIR}")
endif()

find_library(MyoAPI_LIBRARY
             NAMES myo32
             PATHS ENV MYO_ROOT
             PATH_SUFFIXES lib)

if(NOT MyoAPI_FIND_QUIETLY)
    message( STATUS "WaveAPI library: ${MyoAPI_LIBRARY}")
endif()

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set OPENSIM_FOUND to TRUE
# if all listed variables are TRUE
# DEFAULT_MSG is predefined... change only if you need a custom msg
find_package_handle_standard_args(MyoAPI DEFAULT_MSG
                                  MyoAPI_INCLUDE_DIR MyoAPI_LIBRARY)