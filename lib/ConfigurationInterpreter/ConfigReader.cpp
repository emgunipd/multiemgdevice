#include <memory>
#include <iostream>
#include <vector>
#include <string>

#include "ConfigReader.h"
#include "configuration.hxx"

using namespace std;

ConfigReader::ConfigReader()
{}

ConfigReader::ConfigReader(std::string xmlFileName )
{
  try {
    unique_ptr<configuration_t> c(configuration(xmlFileName));

    // how many devices there are in this setup
    currentSetup_.nUsedDevices = c->setup().use().size();

    if (currentSetup_.nUsedDevices == 0) {
      throw string("No devices were selected in the xml file.\n");
    }

    // fill a vector with the names of the used devices
    vector<string> usedDevicesNames;

    for (setup::use_const_iterator i(c->setup().use().begin());
      i != c->setup().use().end();
      ++i)
    {
      usedDevicesNames.push_back(*i);
    }

    
    // parse every device listed in <devices>
    for (devices::device_const_iterator i(c->devices().device().begin());
      i != c->devices().device().end();
      i++)
    {
      // for every device listed in use check if the name matches one on the list,
      // if so push it in the stack
      for (unsigned int j = 0; j < currentSetup_.nUsedDevices; j++)
      {
        if (i->name() == usedDevicesNames[j])
        {
          // add the number of emg channels to nEmgTot
          currentSetup_.nEmgTot += i->emg()->noChannels();

          //add the number of accelerometer channels to nAccTot
          currentSetup_.nAccTot += i->accelerometer()->noChannels();
          if (currentSetup_.nAccTot > 0)
            currentSetup_.nAccTot = 3;
          else
            currentSetup_.nAccTot = 3;

          // store this device to the currentSetup list usedDeviceList
          config::deviceDescriptor dev;

          // TODO: MAKE IT POSSIBLE NOT TO DEFINE ALL THE SENSORS 
          dev.deviceClass = i->deviceClass();
          dev.name = i->name();
          dev.nEmg = i->emg()->noChannels();
          dev.nAccelerometer = i->accelerometer()->noChannels();
          dev.nOrientation = i->orientation()->noChannels();
          dev.nGyroscope = i->gyroscope()->noChannels();
          dev.dummy = i->dummy();
          if (dev.frequency <= 0) {
            cout << i->name() << " has a 0 or negative frequency. Please fix and restart." << endl;
            return;
          }
          dev.frequency = i->frequency();

          // evaluate what's the maximum frequency between the devices
          if (dev.frequency > currentSetup_.recordingFrequency)
            currentSetup_.recordingFrequency = dev.frequency;

          currentSetup_.usedDevicesList.push_back(dev);
        }
      }
    }
   
    // fill the currentSetup structure with the option and socketConfig informations

    currentSetup_.ip = c->socketConfig().ip().get();
    currentSetup_.serverIp = c->socketConfig().serverIp().get();
    currentSetup_.commandPort = c->socketConfig().commandPort().get();
    currentSetup_.emgPort = c->socketConfig().emgPort().get();
    currentSetup_.accelerometerPort = c->socketConfig().accelerometerPort().get();
    currentSetup_.bufferRows = c->socketConfig().bufferRows().get();
    currentSetup_.logData = c->options().logData().get();
    currentSetup_.logDataSeparately = c->options().logDataSeparately().get();
    currentSetup_.socketCommunication = c->options().socketCommunication().get();
    currentSetup_.showData = c->options().showData().get();
    currentSetup_.showStatistics = c->options().showStatistics().get();

  }
  catch (const xml_schema::exception& e)
  {
    cerr << e << endl;
    int i;
    cin >> i;
  }
}

struct config::currentSetup& ConfigReader::getCurrentSetup()
{
  // returns a copy of the private struct
  // is there any other better way to do this?
  return currentSetup_;
}


ConfigReader::~ConfigReader()
{
}
