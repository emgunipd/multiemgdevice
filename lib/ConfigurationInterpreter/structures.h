#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <string>
#include <vector>

namespace config{

  struct deviceDescriptor {
    std::string name;
    std::string deviceClass;
    unsigned int nEmg = 0;
    unsigned int nAccelerometer = 0;
    unsigned int nOrientation = 0;
    unsigned int nGyroscope = 0;
    bool dummy = false;
    unsigned int frequency = 2048;
  };

  
  struct currentSetup{
    std::vector<deviceDescriptor> usedDevicesList;
    std::string ip;
    std::string serverIp;
    unsigned int commandPort = 50040;
    unsigned int emgPort = 50041;
    unsigned int accelerometerPort = 50042;
    unsigned int bufferRows = 1;
    bool logData = false;
    bool logDataSeparately = false;
    bool socketCommunication = true;
    bool showData = false;
    bool showStatistics = false;
    unsigned int nUsedDevices = 0;
    unsigned int nEmgTot = 0;
    unsigned int nAccTot = 0;
    unsigned int recordingFrequency = 1;
  };
  

 /* struct currentSetup {
    int nUsedDevices = 0;
    std::vector<deviceDescriptor> usedDevicesList;
  };*/
}

#endif