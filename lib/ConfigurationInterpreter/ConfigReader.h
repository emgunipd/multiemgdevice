#ifndef CONFIGREADER_H
#define CONFIGREADER_H

#include <vector>
#include <string>
//#include "Device.h"
#include "structures.h"

class ConfigReader
{
public:
  ConfigReader();
  ConfigReader(std::string xmlFileName);
  ~ConfigReader();
  struct config::currentSetup& getCurrentSetup();

private:
  struct config::currentSetup currentSetup_;
};

#endif