#ifndef SOCKET_H
#define SOCKET_H

#include "ceinms/Concurrency/Queue.h"
#include "ceinms/Concurrency/Latch.h"
#include "SharedVariables.h"
#include "DataFrame.h"
#include "structures.h"
#include <string>
#include <thread>
#include <chrono>
#include <fstream>
#include <vector>
#include <boost\asio.hpp>

class Socket
{
public:
  Socket() = delete;
  Socket(CEINMS::Concurrency::Queue<DataFrame>& dataQueue, 
    CEINMS::Concurrency::Latch& mLatch, 
    std::string dataType, 
    config::currentSetup setup);
  virtual ~Socket();
  void operator()();

  int initializeCmd();
  int initializeData();
  
  bool isStreamEnabled();

  void close();

private:

  void flush();
  std::string floatToString(float f);
  void queue(DataFrame& data);
  config::currentSetup setup_;
  //const string dataName_;
  
  int bufferRows_;
  int COMMAND_PORT;
  int DATA_PORT;
  int nData_;
  std::string dataName_;
  CEINMS::Concurrency::Queue<DataFrame>& dataQueue_;
  CEINMS::Concurrency::Latch& mLatch_;
  std::vector<DataFrame> buffer_;
  boost::asio::io_service io_service_;
  boost::asio::ip::tcp::acceptor* acceptorCmd_;
  boost::asio::ip::tcp::acceptor* acceptorData_;
  boost::asio::ip::tcp::socket* socketCmd_;
  boost::asio::ip::tcp::socket* socketData_;

  // indicates whether NinaPro said "ok start streaming through socket" or not
  bool streamEnabled;
};

#endif