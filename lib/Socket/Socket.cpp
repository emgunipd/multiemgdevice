#include <iostream>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include "SharedVariables.h"
#include "Socket.h"
#include "ConfigReader.h"

Socket::Socket(CEINMS::Concurrency::Queue<DataFrame>& dataQueue,
  CEINMS::Concurrency::Latch& mLatch, std::string dataType, config::currentSetup setup) :
  dataQueue_(dataQueue), mLatch_(mLatch), dataName_(dataType), setup_(setup)
{}

void Socket::operator()()
{
  dataQueue_.subscribe();

  // initialize socket
  if (dataName_ == "emg")
  {
    COMMAND_PORT = 50039;
    DATA_PORT = setup_.emgPort;
    nData_ = setup_.nEmgTot;
  }
  else if (dataName_ == "acc")
  {
    COMMAND_PORT = 50040;
    DATA_PORT = setup_.accelerometerPort;
    nData_ = setup_.nAccTot;
  }
  bufferRows_ = setup_.bufferRows;
  streamEnabled = false;
  initializeCmd();
  initializeData();

  mLatch_.wait();

  while (runCondition.get()) {
    queue(dataQueue_.pop());
  }
}

int Socket::initializeCmd()
{
  // command socket initialization. Just called once for the two kind of data. It repeats two times its body
  try
  {
    boost::asio::io_service io_service_cmd;
    acceptorCmd_ = new boost::asio::ip::tcp::acceptor(io_service_cmd, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), COMMAND_PORT));
    // std::cout << dataName_ << ": command_port=" << COMMAND_PORT << " data_port=" << DATA_PORT << std::endl;

    socketCmd_ = new boost::asio::ip::tcp::socket(io_service_cmd);
   

    std::cout << dataName_ << ": CMD socket opened\n";
    acceptorCmd_->accept(*socketCmd_);
    std::cout << dataName_ << ": host connected to CMD\n";

    // NinaPro requests to receive these empty lines
    boost::asio::write(*socketCmd_, boost::asio::buffer("\n", 1));
    boost::asio::write(*socketCmd_, boost::asio::buffer("\n", 1));

    // read buffer
    boost::array<char, 128> buf;
    boost::system::error_code error;

    // FIRST READ (NinaPro sends OK when ready, MultiEmgDevice answers with OK)
    size_t len = socketCmd_->read_some(boost::asio::buffer(buf), error);

    // show what NinaPro sent
    // std::cout.write(buf.data(), len);

    // START COMMAND
    std::string startString("START");
    std::string dataReceived(buf.begin(), buf.end());

    if (dataReceived.find(startString) != std::string::npos)
    {
      std::cout << dataName_ << ": START received\n";
      boost::asio::write(*socketCmd_, boost::asio::buffer("OK\n"));
      boost::asio::write(*socketCmd_, boost::asio::buffer("\n", 1));
      streamEnabled = true;
      std::cout << dataName_ << ": OK sent\n\n";
    }

    // SECOND READ: NinaPro sends what kind of data is requiring, MultiEmgDevice answers  
    // with the number of data to send and the recording frequency

    len = socketCmd_->read_some(boost::asio::buffer(buf), error);
    dataReceived.clear();
    dataReceived.assign(buf.begin(), buf.end());

    // ACC COMMAND
    std::string accString("ACC");
    if (dataReceived.find(accString) != std::string::npos)
    {
      //std::this_thread::sleep_for(std::chrono::milliseconds(3000));
      // number of data to send
      std::string dataString = std::to_string(nData_) + '\n';
      std::cout << dataName_ << ": ACC received\n";
      boost::asio::write(*socketCmd_, boost::asio::buffer(dataString));
      boost::asio::write(*socketCmd_, boost::asio::buffer("\n", 1));
      std::cout << dataName_ << ": n. " << nData_ << "  sent\n\n";

      // recording frequency (we make NinaPro record emg ad acc with the same frequency, 
      // because it would be oversampled anyway in postprocessing)
      dataString = std::to_string(setup_.recordingFrequency) + '\n';
      boost::asio::write(*socketCmd_, boost::asio::buffer(dataString));
      boost::asio::write(*socketCmd_, boost::asio::buffer("\n", 1));

      std::cout << dataName_ << " setup_.recordingFrequency:" << setup_.recordingFrequency << std::endl;

    }

    // EMG COMMAND
    std::string emgString("EMG");

    if (dataReceived.find(emgString) != std::string::npos)
    {
      
      std::string dataString = std::to_string(nData_) + '\n';
      std::cout << dataName_ << ": EMG received\n";
      boost::asio::write(*socketCmd_, boost::asio::buffer(dataString));
      boost::asio::write(*socketCmd_, boost::asio::buffer("\n", 1));
      std::cout << dataName_ << ": n. " << nData_ << "  sent\n";

      // recording frequency (we make NinaPro record emg and acc with the same frequency, 
      // because it would be oversampled anyway in postprocessing)
      dataString = std::to_string(setup_.recordingFrequency) + '\n';
      boost::asio::write(*socketCmd_, boost::asio::buffer(dataString));
      boost::asio::write(*socketCmd_, boost::asio::buffer("\n", 1));
      std::cout << dataName_ << " setup_.recordingFrequency:" << setup_.recordingFrequency << std::endl;
    }
    // std::cout << dataName_ << ": Buffer data: ";
    // std::cout.write(buf.data(), len);

    if (error == boost::asio::error::eof)
      return 1; // Connection closed cleanly by peer.
    else if (error)
    {
      throw boost::system::system_error(error); // Some other error.
      return 0;
    }

    /////// TODO: QUIT COMMAND

    // quit the auxiliary "command" communication
    // if (socket_cmd.is_open()) {
    //   socket_cmd.close();
    //   std::cout << dataName_ << ": closing socket_cmd\n";
    //   }

    //   if (acceptor_cmd.is_open()) {
    //   acceptor_cmd.close();
    //   std::cout << dataName_ << ": closing acceptor_cmd\n";
    //   }

    //std::cout << dataName_ << ": cmd closed...\n";
  }
  catch (std::exception& e)
  {
    std::cerr << dataName_ << ": Exception: " << e.what() << std::endl;
    return 0;
  }
  return 1;
}

// Initialize data sockets in a member object
int Socket::initializeData()
{
  try {
    acceptorData_ = new boost::asio::ip::tcp::acceptor(io_service_, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), DATA_PORT));
    socketData_ = new boost::asio::ip::tcp::socket(io_service_);
    std::cout << dataName_ << ": data socket opened\n";
    acceptorData_->accept(*socketData_);
    std::cout << dataName_ << ": host connected to data socket\n";
  }
  catch (std::exception& e)
  {
    std::cerr << dataName_ << ": Exception: " << e.what() << std::endl;
    return 0;
  }

  std::cout << std::endl << dataName_ << ": data socket opened and ready to transmit \n";
  return 1;
}

std::string Socket::floatToString(float f)
{
  // converts float value into bytes (4 unsigned char) and then to a string.
  std::string str;
  const char* pc = reinterpret_cast<char*>(&f);
  int num = 1;

  // we must convert floats with bytes 
  if (sizeof(float) != 4)
    return "NULL";

  // apparently don't care about endianess...
  for (int i = 0; i < 4; i++)
  {
    //   std::cout << pc[i];
    str += pc[i];
  }

  return str;
}

// Queue method will fill the buffer and when it's full it will flush it
void Socket::queue(DataFrame& data)
{
  buffer_.push_back(data);
  // cout << "buffer size: " << buffer_.size() << endl;
  if (buffer_.size() >= bufferRows_)
  {
    flush();
    buffer_.empty();
  }
}

void Socket::close()
{

}

bool Socket::isStreamEnabled()
{
  return streamEnabled;
}

void Socket::flush()
{
  std::string bufferString;
  for (unsigned int row = 0; row < buffer_.size(); row++)
  {
    for (int col = 0; col < nData_; col++)
      bufferString += floatToString(buffer_.at(row).data.at(col));
  }

  try{
    boost::system::error_code error;
    socketData_->write_some(boost::asio::buffer(bufferString), error);
    buffer_.erase(buffer_.begin(), buffer_.end());

    if (error)
    {
      if (socketData_->is_open()) {
        socketData_->close();
        std::cout << dataName_ << ": closing socket_cmd\n";
      }

      if (acceptorData_->is_open()) {
        acceptorData_->close();
        std::cout << dataName_ << ": closing acceptor_cmd\n";
      }

      std::cout << dataName_ << ": cmd closed...\n";
      throw boost::system::system_error(error);
    }
  }
  catch (std::exception& e)
  {
    std::cerr << dataName_ << ": Exception: " << e.what() << std::endl;
    return; 
  }
}

Socket::~Socket()
{
  // quit the auxiliary "command" communication
   if (socketCmd_->is_open()) {
     socketCmd_->close();
     std::cout << dataName_ << ": closing socketCmd\n";
     }

     if (acceptorCmd_->is_open()) {
     acceptorCmd_->close();
     std::cout << dataName_ << ": closing acceptorCmd\n";
     }

     if (socketData_->is_open()) {
       socketData_->close();
       std::cout << dataName_ << ": closing socketData\n";
     }

     if (acceptorData_->is_open()) {
       acceptorData_->close();
       std::cout << dataName_ << ": closing acceptorData\n";
     }


     delete socketCmd_;
     delete acceptorCmd_;
     delete socketData_;
     delete acceptorData_;
}