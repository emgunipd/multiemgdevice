#ifndef DUMMYDEVICE_H
#define DUMMYDEVICE_H


class DummyDevice final 
{
public:
  DummyDevice(config::deviceDescriptor& d);
  ~DummyDevice(void);

private:
  void loop();

  int emgRate_;
  int accRate_;
};

#endif