#ifndef Cometa_h
#define Cometa_h

#include "ceinms/Concurrency/Queue.h"
#include "ceinms/Concurrency/Latch.h"
#include "CometaFrame.h"
#include "structures.h"
#include <string>
#include <thread>
#include <chrono>

class Cometa{
  public:
		Cometa() = delete;
    Cometa(CEINMS::Concurrency::Queue<CometaFrame>& outQueue, 
      CEINMS::Concurrency::Latch& mLatch, 
      config::deviceDescriptor devConfig);
	  virtual ~Cometa();
		void operator()();

private:
    float auxCounter_;
		std::vector<CometaFrame> frames_;
    CEINMS::Concurrency::Queue<CometaFrame>& outQueue_;
    CEINMS::Concurrency::Latch& mLatch_;
    config::deviceDescriptor devConfig_;
    const unsigned int frequency_;
};

#endif
