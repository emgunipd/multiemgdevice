#ifndef SharedVariables_h
#define SharedVariables_h
#include <mutex>
#include "ceinms/Concurrency/Condition.h"
#include "ceinms/Concurrency/Latch.h"

extern CEINMS::Concurrency::Condition runCondition;
extern CEINMS::Concurrency::Latch latch;

#endif
