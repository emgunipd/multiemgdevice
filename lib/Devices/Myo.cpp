#include "Myo.h"
#include "SharedVariables.h"
#include "myo/myo.hpp"
#include "MyoListener.h"

#include <chrono>
#include <thread>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cout;
using std::endl;

using CEINMS::Concurrency::Queue;

Myo::Myo(Queue<MyoFrame>& emgQueue, Queue<MyoFrame>& accQueue, CEINMS::Concurrency::Latch& mLatch, config::deviceDescriptor devConfig) :
emgQueue_(emgQueue), accQueue_(accQueue), mLatch_(mLatch), devConfig_(devConfig), frequency_(devConfig.frequency)
{}

void Myo::operator()(){

  bool dummy = devConfig_.dummy;

  //////////////////////////
  // DUMMY MYO
  if (dummy)
  {
    MyoFrame emgFrame;
    MyoFrame accFrame;
    int nEmg = devConfig_.nEmg;
    int nAcc = devConfig_.nAccelerometer;
    emgFrame.data.resize(nEmg);
    accFrame.data.resize(nAcc);
    auxCounter_ = 0;
    std::chrono::microseconds period(1000000 / frequency_);
    
    mLatch_.wait();

    while (runCondition.get()) {
      for (int i = 0; i < emgFrame.data.size(); i++)
      {
        emgFrame.data.at(i) = auxCounter_;
      }
      for (int i = 0; i < accFrame.data.size(); i++)
      {
        accFrame.data.at(i) = auxCounter_;
      }
      auxCounter_++;
      if (auxCounter_ > frequency_)
        auxCounter_ = 0;

      emgFrame.time = std::chrono::system_clock::now().time_since_epoch().count();
      accFrame.time = emgFrame.time;

      auto start = std::chrono::high_resolution_clock::now();
      auto end = std::chrono::high_resolution_clock::now();
      
      std::this_thread::sleep_for(period);

      //while (std::chrono::duration_cast<std::chrono::microseconds>(end - start) < period)
      //{
       // std::this_thread::yield();
        end = std::chrono::high_resolution_clock::now();
      //}

      emgQueue_.push(emgFrame);
      accQueue_.push(accFrame);
    }
  }

  /////////////////////////////
  // REAL MYO
  else {

    // We catch any exceptions that might occur below -- see the catch statement for more details.
    try
    {
      myo::Hub hub("it.unipd.multiEmgDevice");

      std::cout << "Attempting to find a Myo..." << std::endl;

      myo::Myo* myo = hub.waitForMyo(10000);

      // If waitForMyo() returned a null pointer, we failed to find a Myo, so exit with an error message.
      if (!myo)
      {
        throw std::runtime_error("Unable to find a Myo!");
      }

      std::cout << "Connected to Myo" << std::endl << std::endl;

      // Next we enable EMG streaming on the found Myo.
      
      hub.setLockingPolicy(myo::Hub::lockingPolicyNone);

      // Next we construct an instance of our DeviceListener, so that we can register it with the Hub.
      // We pass to the collector the infos about the way we want to record/stream data 
      MyoListener collector = MyoListener(emgQueue_, accQueue_, mLatch_, devConfig_);

      // Hub::addListener() takes the address of any object whose class inherits from DeviceListener, and will cause
      // Hub::run() to send events to all registered device listeners.
      hub.addListener(&collector);

      mLatch_.wait();
      myo->setStreamEmg(myo::Myo::streamEmgEnabled);
      // polling cycle
      while (runCondition.get())
      {
        hub.run(1);
      }

    }
    catch (const std::exception& e)
    {
      std::cerr << "Error: " << e.what() << std::endl;
      std::cin.ignore();
      return;
    }
  }

}

Myo::~Myo() {
  cout << "Closing Myo Producer..." << endl << std::flush;
}
