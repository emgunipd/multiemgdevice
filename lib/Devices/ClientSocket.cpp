#include <iostream>
#include <string>
#include <array>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include "ClientSocket.h"
#include "structures.h"
#include "SharedVariables.h"
#include "MyoFrame.h"


using boost::asio::ip::tcp;

ClientSocket::ClientSocket(CEINMS::Concurrency::Queue<MyoFrame>& emgQueue,
  CEINMS::Concurrency::Latch& mLatch,
  config::currentSetup setup) : emgQueue_(emgQueue), mLatch_(mLatch), setup_(setup)
{}


void ClientSocket::operator()()
{
  nEmg_ = 8;

  int printCounter = 0;

  emgVector_.data.resize(nEmg_);

  try
  {

    std::string ip;
    boost::asio::io_service io_service;

//    std::cout << "please insert server ip:\n";
  //  std::cin >> ip;
    ip = setup_.serverIp;

    tcp::resolver resolver(io_service);
    tcp::resolver::query query(ip, "daytime");
    tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
    tcp::resolver::iterator end;

    tcp::socket socket(io_service);
    boost::system::error_code error = boost::asio::error::host_not_found;
    while (error && endpoint_iterator != end)
    {
      socket.close();
      socket.connect(*endpoint_iterator++, error);
    }
    if (error)
      throw boost::system::system_error(error);

    // idle until every thread arrived to this point
    mLatch_.wait();

    char ok = '1';
    socket.write_some(boost::asio::buffer(&ok, 1));
    std::cout << "ok sent\n";

    for (;;)
    {
      std::array<int8_t, 8> buf;
      boost::system::error_code error;

      size_t len = socket.read_some(boost::asio::buffer(buf, 8), error);

      if (error == boost::asio::error::eof)
        break; // Connection closed cleanly by peer.
      else if (error)
        throw boost::system::system_error(error); // Some other error.

      for (int i = 0; i < 8; i++){
        emgVector_.data.at(i) = static_cast<float> (buf.at(i));
      }


      // Show statistics
      /*
      if (printCounter == 200) {
        std::cout << emgVector_.data.at(1) << std::endl;
        printCounter = 0;
      }
      else
        printCounter++;
        */
      emgVector_.time = std::chrono::system_clock::now().time_since_epoch().count();
      emgQueue_.push(emgVector_);

    }
  }
  catch (std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }

  return;
}

ClientSocket::~ClientSocket(){
  
}