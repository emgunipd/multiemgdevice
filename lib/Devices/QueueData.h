#ifndef QueueData_h
#define QueueData_h

template <typename T>
	struct QueueData {
		double time;
		T data;
	}; 
#endif
