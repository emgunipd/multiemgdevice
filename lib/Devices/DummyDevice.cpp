#include <iostream>
#include <iomanip>
#include <array>
#include <fstream>
#include <sstream>
#include <vector>
#include <chrono>
#include <ctime>
#include <thread>

#include "DummyDevice.h"
#include "ConfigReader.h"


DummyDevice::DummyDevice(config::deviceDescriptor& d) : Device(d)
{
  cout << "DummyDevice created\n";
  loop();
}

void DummyDevice::loop(){

  // initialize
  // outputMode is set by the base class Device through the passed argument config
  /*if (pConfig_->isLogRequired()) {
    std::cout << "Opening files...\n";
    openFiles();
    std::cout << "Files open\n";
  }

  if (pConfig_->isSocketRequired()) {
    std::cout << "Opening Sockets...\n";
    openSocket();
    std::cout << "Sockets open\n";
  }

  std::chrono::microseconds timestamp;

  vector<int> emg(nEmg_, 0);
  vector<float> acc(nAcc_, 0);
  vector<float> gyro(nGyr_, 0);
  vector<float> orien(nOri_, 0);

  srand(static_cast<unsigned int>(std::time(NULL)));

  // fake polling cycle
  while (1)
  {

    timestamp = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());

    for (unsigned int i = 0; i < emg.size(); i++) {
      emg[i] = static_cast<int>( rand() % 255);
    }
    for (unsigned int i = 0; i < acc.size(); i++) {
      acc[i] = static_cast<float>(i+1);
    }
    for (unsigned int i = 0; i < gyro.size(); i++) {
      gyro[i] = static_cast<float>(i + 1);
    }
    for (unsigned int i = 0; i < orien.size(); i++) {
      orien[i] = static_cast<float>(i + 1);
    }

    if (pConfig_->isLogRequired()) {
      // Log data into files

      
      // emg
      emgFile_ << timestamp.count();
      for (unsigned int i = 0; i < emg.size(); i++)
        emgFile_ << "," << static_cast<int>(emg[i]);
      emgFile_ << endl;
      
      // acc
      accelerometerFile_ << timestamp.count();
      for (unsigned int i = 0; i < acc.size(); i++)
        accelerometerFile_ << "," << static_cast<int>(acc[i]);
      accelerometerFile_ << endl;

      // gyro
      gyroFile_ << timestamp.count();
      for (unsigned int i = 0; i < gyro.size(); i++)
        gyroFile_ << "," << static_cast<int>(gyro[i]);
      gyroFile_ << endl;

      // ori
      orientationFile_ << timestamp.count();
      for (unsigned int i = 0; i < orien.size(); i++)
        orientationFile_ << "," << static_cast<int>(orien[i]);
      orientationFile_ << endl;
    }


    if (pConfig_->isPrintRequired()) {
      // we print emg values only
      std::cout << "\r";
      for (unsigned int i = 0; i < emg.size(); i++) {
        std::cout << std::setw(4) << static_cast<int>(emg[i]);
      }
    }

    if (pConfig_->isSocketRequired() && emgSocket_->isStreamEnabled()) {
      for (unsigned int i = 0; i < emg.size(); i++) {
        emgSocket_->queue(static_cast<float>(emg[i]) / 1000);
      }
      for (unsigned int i = 0; i < acc.size(); i++) {
        accSocket_->queue(acc[i] / 1000);
      }
    }

    // sleep for 100 ms (10 hz) 
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
  }*/
}


DummyDevice::~DummyDevice(void)
{
}