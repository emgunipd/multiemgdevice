#include "Cometa.h"
#include "SharedVariables.h"
#include "WaveAPI.h"
#include <windows.h>
#include <chrono>
#include <thread>
#include <string>
#include <stdio.h>
#include <conio.h>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cout;
using std::endl;

using CEINMS::Concurrency::Queue;

Cometa::Cometa(Queue<CometaFrame>& outQueue, CEINMS::Concurrency::Latch& mLatch, config::deviceDescriptor devConfig) :
outQueue_(outQueue), mLatch_(mLatch), devConfig_(devConfig), frequency_(devConfig.frequency) 
{ }

void Cometa::operator()(){

  bool dummy = devConfig_.dummy;

  cout << "starting Cometa...\n";
  //////////
  // DUMMY COMETA

  if (dummy) {
    auxCounter_ = 0;
    std::chrono::microseconds period(1000000 / frequency_);

    CometaFrame aFrame;
    aFrame.data.resize(devConfig_.nEmg);
    // the unit of timestamp is 1/10 microseconds

    mLatch_.wait();

    while (runCondition.get()) {
      auto start = std::chrono::high_resolution_clock::now();
      auto end = std::chrono::high_resolution_clock::now();

      std::this_thread::sleep_for(period);

      //while (std::chrono::duration_cast<std::chrono::microseconds>(end - start) < period)
      //{
       // std::this_thread::yield();
        end = std::chrono::high_resolution_clock::now();
      //}

      auxCounter_ = auxCounter_ + 1;
      if (auxCounter_ > frequency_)
        auxCounter_ = 0;

      for (int i = 0; i < aFrame.data.size(); i++)
      {
        aFrame.data.at(i) = auxCounter_;
      }
      
      aFrame.time = std::chrono::system_clock::now().time_since_epoch().count();
      outQueue_.push(aFrame);
    }
  }
  /////////////
  // REAL COMETA
  else {
    int errcode;
    int emgInstalledChanNum = MAX_EMG_INSTALLED_CHAN;
    int emgActivatedChan = devConfig_.nEmg;
    int fswInstalledChanNum = 2;
    int fswChanEnableVect[MAX_FSW_INSTALLED_CHAN];
    int fswThresholdVect[MAX_FSW_INSTALLED_CHAN * FSW_TRANSDUCERS];
    int fswProtocolType = FSW_PROT_QUARTER_FOOT;
    int trgMode = DAQ_TRG_OUT_ENABLE;
    short int * dataBuff;
    unsigned int dataNum;
    int isAcqRunning;
    int startFromTrg, stopFromTrg;
    unsigned int firstSample, lastSample;
    WaveDevice *device = NULL;

    int emgChanEnableVect[MAX_EMG_INSTALLED_CHAN];
    device = CreateWaveDevice();
    while (device == NULL){
      std::cout << "device creation error" << std::endl;
      device = CreateWaveDevice();
    }
    // Configure acquisition parameters
    //enable the Emg channels (1= enabled, 0= disabled) 
    for (int i = 0; i < emgInstalledChanNum; i++)
    {
      if (i < emgActivatedChan)
        emgChanEnableVect[i] = 1;
      else
        emgChanEnableVect[i] = 0;
      std::cout << " ch. no: " << i << "enabled: " << emgChanEnableVect[i] << std::endl;
    }
    //enable all the FootSw channels (1= enabled, 0= disabled) 
    for (int i = 0; i < fswInstalledChanNum; i++)
    {
      fswChanEnableVect[i] = 0;
      //set the FootSw transducers threshold
      for (int t = 0; t < FSW_TRANSDUCERS; t++)
        fswThresholdVect[i*FSW_TRANSDUCERS + t] = 20;
    }
    errcode = device->configure(emgChanEnableVect, fswChanEnableVect, fswThresholdVect, fswProtocolType, trgMode);
    if (errcode != 0)
      std::cout << "Error in configuring cometa. Error code: " << errcode << std::endl;
    if ((errcode = device->activate()) != 0)
      printf("\nError executing activate() method - Error code= %d\n", errcode);

    CometaFrame aFrame;
    aFrame.data.resize(emgActivatedChan);

    mLatch_.wait();
    errcode = device->run();

    while ((!_kbhit() || _getch() != 27) && (errcode == 0) && runCondition.get())
    {
      errcode = device->transferData(&dataBuff, &dataNum, &isAcqRunning, &startFromTrg, &stopFromTrg, &firstSample, &lastSample);
      if (errcode == 0)
      {
        for (int t = 0; t < dataNum / emgActivatedChan; t++){
          for (int i = t*emgActivatedChan; i < emgActivatedChan*(t + 1); i++){
            aFrame.data.at(i - t*emgActivatedChan) = dataBuff[i];
            aFrame.time = std::chrono::system_clock::now().time_since_epoch().count();
          }
          outQueue_.push(aFrame);
        }
      }
    }
    cout << "cometa exiting" << endl;
  }
}

Cometa::~Cometa() {
  cout << "Closing Cometa Producer..." << endl << std::flush;
}
