#ifndef DataFrame_h
#define DataFrame_h


#include "QueueData.h"
#include <vector>

typedef QueueData<std::vector<float>> DataFrame;

#endif
