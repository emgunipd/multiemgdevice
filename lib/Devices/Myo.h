#ifndef Myo_h
#define Myo_h

#include "ceinms/Concurrency/Queue.h"
#include "ceinms/Concurrency/Latch.h"
#include "MyoFrame.h"
#include "structures.h"
#include <string>
#include <thread>
#include <chrono>

class Myo{
  public:
		Myo() = delete;
    Myo(CEINMS::Concurrency::Queue<MyoFrame>& emgQueue, 
      CEINMS::Concurrency::Queue<MyoFrame>& accQueue, 
      CEINMS::Concurrency::Latch& mLatch, 
      config::deviceDescriptor devConfig);
	  virtual ~Myo();
		void operator()();

private:
    int auxCounter_;
    CEINMS::Concurrency::Queue<MyoFrame>& emgQueue_;
    CEINMS::Concurrency::Queue<MyoFrame>& accQueue_;
    CEINMS::Concurrency::Latch& mLatch_;
    config::deviceDescriptor devConfig_;
    const unsigned int frequency_;
};

#endif
