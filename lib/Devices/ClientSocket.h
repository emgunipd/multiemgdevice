#ifndef CLIENTSOCKET_H
#define CLIENTSOCKET_H

#include <string>
#include <thread>
#include <chrono>
#include <vector>
#include "ClientSocket.h"
#include "MyoFrame.h"
#include "ceinms\Concurrency\Queue.h"
#include "ceinms\Concurrency\Latch.h"
#include "SharedVariables.h"
#include "structures.h"

class ClientSocket {

public:
  ClientSocket() = delete; 
  ClientSocket(CEINMS::Concurrency::Queue<MyoFrame>& outQueue,
    CEINMS::Concurrency::Latch& mLatch,
    config::currentSetup setup);
  virtual ~ClientSocket();
  void operator()();

private:
  MyoFrame emgVector_;
  CEINMS::Concurrency::Queue<MyoFrame>& emgQueue_;
  CEINMS::Concurrency::Latch& mLatch_;
  config::currentSetup setup_;

  int nEmg_;
};

#endif