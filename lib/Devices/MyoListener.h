#ifndef MYOLISTENER_H
#define MYOLISTENER_H

#include "ceinms/Concurrency/Queue.h"
#include "ceinms/Concurrency/Latch.h"
#include "myo\myo.hpp"
#include "MyoFrame.h"
#include "structures.h"
#include <vector>

class MyoListener : public myo::DeviceListener
{
public:
  MyoListener() = delete;
  MyoListener(CEINMS::Concurrency::Queue<MyoFrame>& emgQueue, CEINMS::Concurrency::Queue<MyoFrame>& accQueue, 
    CEINMS::Concurrency::Latch& mLatch, config::deviceDescriptor devConfig);
  void onEmgData(myo::Myo* myo, uint64_t timestamp, const int8_t* emg);
  void onOrientationData(myo::Myo *myo, uint64_t timestamp, const myo::Quaternion< float > &rotation);
  void onAccelerometerData(myo::Myo *myo, uint64_t timestamp, const myo::Vector3< float > &accel);
  void onGyroscopeData(myo::Myo *myo, uint64_t timestamp, const myo::Vector3< float > &gyro);
  void onConnect(myo::Myo *myo, uint64_t timestamp, myo::FirmwareVersion firmwareVersion);
  ~MyoListener();
private:
  void fillMap();
  CEINMS::Concurrency::Queue<MyoFrame>& emgQueue_;
  CEINMS::Concurrency::Queue<MyoFrame>& accQueue_;
  CEINMS::Concurrency::Latch& mLatch_;
  config::deviceDescriptor devConfig_;
  MyoFrame emgVector_;
  MyoFrame accVector_;
  const short int ADhalfResolution_ = 128;
  std::map<int, int> electrodesMap_;

};
#endif