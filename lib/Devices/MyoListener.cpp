#include "MyoListener.h"
#include <iostream>
using std::cout;
using std::endl;

using CEINMS::Concurrency::Queue;

MyoListener::MyoListener(CEINMS::Concurrency::Queue<MyoFrame>& emgQueue, CEINMS::Concurrency::Queue<MyoFrame>& accQueue, 
  CEINMS::Concurrency::Latch& mLatch, config::deviceDescriptor devConfig) :
  emgQueue_(emgQueue), accQueue_(accQueue), mLatch_(mLatch), devConfig_(devConfig)
{
  cout << "MyoListener created\n";
  fillMap();
  int nEmg = devConfig_.nEmg;
  int nAcc = devConfig_.nAccelerometer;
  emgVector_.data.resize(nEmg);
  accVector_.data.resize(nAcc);
}

// onEmgData() is called whenever a paired Myo has provided new EMG data, and EMG streaming is enabled.
// the unit of timestamp is microseconds.

void MyoListener::onEmgData(myo::Myo* myo, uint64_t timestamp, const int8_t* emg)
{
  for (int i = 0; i < emgVector_.data.size(); i++) {
    // REMOVE MAPPING TO PERFORM COMETA ONLY ACQ // is it necesssary?
    emgVector_.data.at(electrodesMap_.at(i)) = static_cast<float>(emg[i]);
  }
  emgVector_.time = std::chrono::system_clock::now().time_since_epoch().count();
  emgQueue_.push(emgVector_);
}

void MyoListener::onAccelerometerData(myo::Myo *myo, uint64_t timestamp, const myo::Vector3< float > &accel)
{
  accVector_.data.at(0) = static_cast<float>(accel.x());
  accVector_.data.at(1) = static_cast<float>(accel.y());
  accVector_.data.at(2) = static_cast<float>(accel.z());
  accVector_.time = std::chrono::system_clock::now().time_since_epoch().count();
  accQueue_.push(accVector_);
}

void MyoListener::fillMap(){
  electrodesMap_.emplace(0, 3);
  electrodesMap_.emplace(1, 2);
  electrodesMap_.emplace(2, 1);
  electrodesMap_.emplace(3, 0);
  electrodesMap_.emplace(4, 7);
  electrodesMap_.emplace(5, 6);
  electrodesMap_.emplace(6, 5);
  electrodesMap_.emplace(7, 4);
}


void MyoListener::onOrientationData(myo::Myo *myo, uint64_t timestamp, const myo::Quaternion< float > &rotation){}
void MyoListener::onGyroscopeData(myo::Myo *myo, uint64_t timestamp, const myo::Vector3< float > &gyro) {}
void MyoListener::onConnect(myo::Myo *myo, uint64_t timestamp, myo::FirmwareVersion firmwareVersion) {}
MyoListener::~MyoListener(){}
