#ifndef Manager_h
#define Manager_h

#include "ceinms/Concurrency/Queue.h"
#include "ceinms/Concurrency/Latch.h"
#include "DataFrame.h"
#include "MyoFrame.h"
#include "CometaFrame.h"
#include "structures.h"
#include <string>
#include <vector>
#include <thread>
#include <chrono>

class Manager{
  public:
		Manager() = delete;
		Manager(CEINMS::Concurrency::Queue<MyoFrame>& myoEmgQueue,
      CEINMS::Concurrency::Queue<MyoFrame>& myoAccQueue,
      CEINMS::Concurrency::Queue<CometaFrame>& cometaQueue,
      CEINMS::Concurrency::Queue<MyoFrame>& clientSocketQueue,
      CEINMS::Concurrency::Queue<DataFrame>& emgQueue,
      CEINMS::Concurrency::Queue<DataFrame>& accQueue,
      CEINMS::Concurrency::Latch& mLatch,
      config::currentSetup setup);
	  virtual ~Manager();
		void operator()();

private:
  CEINMS::Concurrency::Queue<MyoFrame>& myoEmgQueue_;
  CEINMS::Concurrency::Queue<MyoFrame>& myoAccQueue_;
  CEINMS::Concurrency::Queue<CometaFrame>& cometaQueue_;
  CEINMS::Concurrency::Queue<MyoFrame>& clientSocketQueue_;
  CEINMS::Concurrency::Queue<DataFrame>& emgQueue_;
  CEINMS::Concurrency::Queue<DataFrame>& accQueue_;
  CEINMS::Concurrency::Latch& mLatch_;
  config::currentSetup setup_;
};

#endif
