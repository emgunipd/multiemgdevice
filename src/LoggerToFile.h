#ifndef LoggerToFile_h
#define LoggerToFile_h

#include "ceinms/Concurrency/Queue.h"
#include "ceinms/Concurrency/Latch.h"
#include "DataFrame.h"
#include <string>
#include <thread>
#include <chrono>
#include <fstream>

class LoggerToFile{
public:
  LoggerToFile() = delete;
  LoggerToFile(CEINMS::Concurrency::Queue<DataFrame>& emgQueue, CEINMS::Concurrency::Latch& mLatch, const std::string filename);
  virtual ~LoggerToFile();
  void operator()();

private:
  CEINMS::Concurrency::Queue<DataFrame>& emgQueue_;
  CEINMS::Concurrency::Latch& mLatch_;
  std::ofstream emgLogFile_;
};

#endif
