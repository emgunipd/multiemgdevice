#include "Queues.h"

using CEINMS::Concurrency::Queue;

Queue<MyoFrame> myoEmgQueue;
Queue<MyoFrame> myoAccQueue;
Queue<MyoFrame> clientSocketQueue;
Queue<CometaFrame> cometaQueue;
Queue<DataFrame> emgQueue;
Queue<DataFrame> accQueue;