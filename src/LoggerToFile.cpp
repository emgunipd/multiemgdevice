#include "LoggerToFile.h"
#include "SharedVariables.h"

#include <fstream>
using std::ofstream;
#include <chrono>
#include <thread>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cout;
using std::endl;

using CEINMS::Concurrency::Queue;

LoggerToFile::LoggerToFile(Queue<DataFrame>& emgQueue, CEINMS::Concurrency::Latch& mLatch, const std::string filename) :
emgQueue_(emgQueue), mLatch_(mLatch) {
  emgLogFile_.open(filename);
  emgLogFile_.precision(0);
}

void LoggerToFile::operator()(){
  emgQueue_.subscribe();

  mLatch_.wait();

  while (runCondition.get()) {
    auto DataFrame = emgQueue_.pop();
    auto DataFrameData = DataFrame.data;
    auto DataFrameTime = DataFrame.time;

    //cout << DataFrameData.at(0) << endl;
    // THIS *���%$ loses half of the samples in the queue!!!! WHY?
    emgLogFile_ << std::fixed << DataFrameTime;
    for (int i = 0; i < DataFrameData.size(); i++)
    {
      emgLogFile_ << " " << DataFrameData[i];
    }
    emgLogFile_ << endl;
  }

  cout << "logger existing" << endl;
}

LoggerToFile::~LoggerToFile() {
  cout << "Closing Logger..." << endl << std::flush;
  emgLogFile_.close();
}
