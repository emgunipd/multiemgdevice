#include "Manager.h"
#include "SharedVariables.h"


#include <chrono>
#include <thread>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cout;
using std::endl;

using CEINMS::Concurrency::Queue;

Manager::Manager(Queue<MyoFrame>& myoEmgQueue, Queue<MyoFrame>& myoAccQueue, Queue<CometaFrame>& cometaQueue, Queue<MyoFrame>& clientSocketQueue,
  Queue<DataFrame>& emgQueue, Queue<DataFrame>& accQueue, CEINMS::Concurrency::Latch& mLatch, config::currentSetup setup) :
  myoEmgQueue_(myoEmgQueue), myoAccQueue_(myoAccQueue), cometaQueue_(cometaQueue), clientSocketQueue_(clientSocketQueue),
  emgQueue_(emgQueue), accQueue_(accQueue), mLatch_(mLatch), setup_(setup)
{}

void Manager::operator()(){
  cout << "MyoListener called" << endl;
  
  
  bool isMyoUsed = false;
  bool isCometaUsed = false;
  bool isClientSocketUsed = false;
  unsigned int myoFreq = 1;
  unsigned int cometaFreq = 1;
  unsigned int clientSocketFreq = 1;
  unsigned int maxFreq = setup_.recordingFrequency;

  for (int i = 0; i < setup_.nUsedDevices; i++)
  {
    if (setup_.usedDevicesList.at(i).deviceClass == "myo") {
      isMyoUsed = true;
      myoFreq = setup_.usedDevicesList.at(i).frequency;
      myoEmgQueue_.subscribe();
      myoAccQueue_.subscribe();
    }
    else if (setup_.usedDevicesList.at(i).deviceClass == "cometa") {
      isCometaUsed = true;
      cometaFreq = setup_.usedDevicesList.at(i).frequency;
      cometaQueue_.subscribe();
    }
    else if (setup_.usedDevicesList.at(i).deviceClass == "clientsocket") {
      isClientSocketUsed = true;
      clientSocketFreq = setup_.usedDevicesList.at(i).frequency;
      clientSocketQueue_.subscribe();
    }
  }
  mLatch_.wait();

  cout << "\nRecording\n";

  while (runCondition.get()) {
    DataFrame emgManagerFrame;
    DataFrame accManagerFrame;
    MyoFrame myoAccFrame;
    MyoFrame myoEmgFrame;
    CometaFrame cometaFrame;
    MyoFrame clientSocketFrame;
    // Timestamping will be provided by NinaPro


    // This is not great, in fact we should check which device is faster and make a pop on that,
    // and on the other ones we should use popLast
    // TODO: ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    /*
    if (isMyoUsed && isCometaUsed) {
    myoAccFrame = myoAccQueue_.popLast();
    myoEmgFrame = myoEmgQueue_.popLast(); // remove to perform cometa only acq (check)
    }
    else if (isMyoUsed && !isCometaUsed) {
    //myoAccFrame = myoAccQueue_.pop();
    myoEmgFrame = myoEmgQueue_.pop();
    }

    if (isCometaUsed) {
    cometaFrame = cometaQueue_.pop();
    // if Cometa is used timestamping is provided by cometa wich runs faster than myo
    emgManagerFrame.time = cometaFrame.time;
    accManagerFrame.time = cometaFrame.time;
    }
    else {
    emgManagerFrame.time = myoEmgFrame.time;
    accManagerFrame.time = myoEmgFrame.time;
    }*/

    if (isMyoUsed && !isClientSocketUsed && !isCometaUsed) {
      // only myo

      myoAccFrame = myoAccQueue_.pop();
      myoEmgFrame = myoEmgQueue_.pop();
      emgManagerFrame.time = myoEmgFrame.time;
      accManagerFrame.time = myoEmgFrame.time;
    }
    else if (!isMyoUsed && !isClientSocketUsed && isCometaUsed) {
      // only cometa
      cometaFrame = cometaQueue_.pop();
      emgManagerFrame.time = cometaFrame.time;
      accManagerFrame.time = cometaFrame.time;
    }
    else if (!isMyoUsed && isClientSocketUsed && !isCometaUsed) {
      // only myoSocket
      clientSocketFrame = clientSocketQueue_.pop();
      emgManagerFrame.time = clientSocketFrame.time;
      accManagerFrame.time = clientSocketFrame.time;
    }
    else if (isMyoUsed && isClientSocketUsed && !isCometaUsed) {
      // two myos

      // since they have the same speed just pop in sequence.
      myoAccFrame = myoAccQueue_.popLast();
      myoEmgFrame = myoEmgQueue_.pop();
      clientSocketFrame = clientSocketQueue_.pop();
      emgManagerFrame.time = myoEmgFrame.time;
      accManagerFrame.time = myoEmgFrame.time;
    }
    else if (isMyoUsed && !isClientSocketUsed && isCometaUsed) {
      // myo + cometa 

      myoAccFrame = myoAccQueue_.popLast();
      myoEmgFrame = myoEmgQueue_.popLast();
      cometaFrame = cometaQueue_.pop();
      // if Cometa is used timestamping is provided by cometa wich runs faster than myo
      emgManagerFrame.time = cometaFrame.time;
      accManagerFrame.time = cometaFrame.time;
    }
    else if (isMyoUsed && isClientSocketUsed && isCometaUsed) {
      // two myo + cometa PRO configuration

      myoAccFrame = myoAccQueue_.popLast();
      myoEmgFrame = myoEmgQueue_.popLast();
      clientSocketFrame = clientSocketQueue_.popLast();
      cometaFrame = cometaQueue_.pop();
      emgManagerFrame.time = cometaFrame.time;
      accManagerFrame.time = cometaFrame.time;
    }
    
    // REMOVE TO PERFORM COMETA ONLY ACQ (check)

    // Copy the various devices' dataFrames in the ManagerFrame that will be printed or sended.
    // data, if available, will be sended like this: Myo + MyoThroughSocket + Cometa
    if (isMyoUsed)
      emgManagerFrame.data.insert(emgManagerFrame.data.end(), myoEmgFrame.data.begin(), myoEmgFrame.data.end());

    if (isClientSocketUsed)
      emgManagerFrame.data.insert(emgManagerFrame.data.end(), clientSocketFrame.data.begin(), clientSocketFrame.data.end());

    if (isCometaUsed)
      emgManagerFrame.data.insert(emgManagerFrame.data.end(), cometaFrame.data.begin(), cometaFrame.data.end());

    emgQueue_.push(emgManagerFrame);

    // ACC
    if (isMyoUsed)
      accManagerFrame.data.insert(accManagerFrame.data.end(), myoAccFrame.data.begin(), myoAccFrame.data.end());
    accQueue_.push(accManagerFrame);

  }
  cout << "manager exiting" << endl;
}


Manager::~Manager() {
  cout << "Closing Manager..." << endl << std::flush;
}
