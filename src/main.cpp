//   sendMain - an implementation of a single producer multiple consumers
//           with the following constraints:
//           - the consumers can subscribe/unsubscribe to the queue at run time
//           - all the messages MUST be consumed by all the subscribed consumers
//
//   Copyright (C) 2014 Monica Reggiani <monica.reggiani@gmail.com>
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

//Including the required headers and libraries
#include "Myo.h"
#include "Cometa.h"
#include "Manager.h"
#include "LoggerToFile.h"
#include "Socket.h"
#include "Queues.h"
#include "SharedVariables.h"
#include "ConfigReader.h"
#include "structures.h"
#include "ClientSocket.h"
#include <boost/filesystem.hpp>
#include <iostream>
using std::cout;
using std::endl;
using std::flush;
#include <iomanip>
using std::setfill;
using std::setw;
using std::left;
#include <vector>
#include <thread>
using std::thread;
#include <csignal>
#include <string>
using std::string;
#include <memory>
using std::unique_ptr;

using namespace CEINMS::Concurrency;

//A signal is a software interrupt delivered to a process. 
//The operating system uses signals to report exceptional situations to an executing program.
//signalHandler manages the closure of the process in case of signals.
//INPUT: signum = the signal's number
//OTPUT: none
void signalHandler(int signum)
{
  cout << endl << "Closing application" << endl << flush;
  runCondition.set(false);
}

//The main is executed from terminal and it could have at most 2 input parameters:
//INPUT: configPath = path of the XML configuration file
//INPUT: acquisitionRelativePath = path in which the data will be stored
int main(int argc, char** argv)
{
//if a signal is delivered, it is managed by the signnalHandler function.
  std::signal(SIGINT, signalHandler);
  
//Constants to display the software name
  const int w = 32;
  const char s = ' ';

// Displaying the software header:
// ================================
// Multi EMG Device
// ================================
  cout << setfill('=') << setw(w) << "=" << endl;
  cout << setfill(s) << setw(w) << left << "Multi EMG Device" << endl;
  cout << setfill('=') << setw(w) << "=" << endl;

// Definition of the string variables to define the XML configuration file and data storage path 
  string configPath;
  string acquisitionRelativePath;

//If there is NO parameters in input, the configpath and acquisitionRelativePath are set to the default settings
//If there is 1 parameter in input, this defines the acquisitionRelativePath while the configpath is set to the default setting (acquisitionRelativePath = argv[1];) 
//If there are 2 parameters in input, these define both path (acquisitionRelativePath = argv[1]; configPath = argv[2];)
//If there are more than 2 parameters in input, an error is showed
  if (argc == 1) {
    configPath = "../../data/configuration.xml";
    acquisitionRelativePath = "acquisition";
  } else if (argc == 2){
    configPath = "../../data/configuration.xml";
    acquisitionRelativePath = argv[1];
  } else if (argc == 3) {
    configPath = argv[2];
    acquisitionRelativePath = argv[1];
  } else {
    cout << "Too many parameters\nUsage: multiemgdevice.exe SubjectID ../../data/configuration.xml\n";
    return 1;
  }
  
//The Boost.Filesystem library provides facilities to manipulate files and directories, and the paths that identify them.
//A PATH object is created
//in dataDir is stored the parent path of the one stored in configPath;
  boost::filesystem::path p(configPath);
  boost::filesystem::path dataDir = p.parent_path();
  
//The path is set as relative and if it already exists, a "_new" suffix is added
  dataDir += "/";
  dataDir += acquisitionRelativePath;
  while (boost::filesystem::exists(dataDir))
  {
    dataDir += "_new";
  }
  
//The directory is created
  boost::filesystem::create_directories(dataDir);

// Configuration object: ConfigReader object get information from the XML configuration file collecting them in a structure.
  ConfigReader configuration(configPath);
//config information are stored into the setup structure
  config::currentSetup setup = configuration.getCurrentSetup();
  
//The field nUsedDevices contains the number of active recording devices  
//If the active device number is lower than 1 or greater than 3, error messages are shown.
  if (setup.nUsedDevices < 1) {
    cout << "No device is configured.\nPlease add a device in configuration.xml\n";
    return 1;
  }
  else if (setup.nUsedDevices > 3) {
    cout << "MultiEmgDevice doesn\'t support more than 3 devices\n";
    return 1;
  }

//Active devices variables initialization
  bool isCometaUsed = false;
  bool isMyoUsed = false;
  bool isClientSocketUsed = false;
//Number of active threads variable initialization
  unsigned short int activeThreads = 1;
//The position of the device in the configuration file is assigned as its index
  unsigned short int myoIndex = 0;
  unsigned short int cometaIndex = 0;
  unsigned short int clientSocketIndex = 0;

//The software search for the active devices, setting the configuration variables(isUsed, activeThreads, Index) 
  for (int i = 0; i < setup.nUsedDevices; i++)
  {
    if (setup.usedDevicesList.at(i).deviceClass == "myo") {
      isMyoUsed = true;
      activeThreads++;
      myoIndex = i;
    }
    else if (setup.usedDevicesList.at(i).deviceClass == "cometa") {
      isCometaUsed = true;
      activeThreads++;
      cometaIndex = i;
    }
    else if (setup.usedDevicesList.at(i).deviceClass == "clientsocket") {
      isClientSocketUsed = true;
      activeThreads++;
      clientSocketIndex = i;
    }
  }

//If in the XML configuration file is also set to activate the socket communication (for the double Myo use)
//2 threads are added (EMG + ACC)
  if (setup.socketCommunication) {
    activeThreads += 2;
  }
//If in the XML configuration file is also set to log the data TOGETHER 1 more thread is added
  if (setup.logData) {
    activeThreads += 1;
  }
//If in the XML configuration file is also set to log the data SEPARATELY a thread FOR EACH DEVICE is added
  if (setup.logDataSeparately) {
    activeThreads += setup.nUsedDevices;
  }

  ////////
  // DISPLAY CONFIGURATION
  ////////
  cout << setfill('-') << setw(w) << "-" << endl;
  for (unsigned short int i = 0; i < setup.nUsedDevices; i++) {
    cout << setfill(s) << setw(w) << left << "Device name: " << setup.usedDevicesList.at(i).name << endl;
    cout << setfill(s) << setw(w) << left << " class: " << setup.usedDevicesList.at(i).deviceClass << endl;
    cout << setfill(s) << setw(w) << left << " dummy: " << setup.usedDevicesList.at(i).dummy << endl;
    cout << setfill(s) << setw(w) << left << " frequency: " << setup.usedDevicesList.at(i).frequency << endl;
  }
  cout << setfill(s) << setw(w) << left << "File logging enabled: " << setup.logData << endl;
  cout << setfill(s) << setw(w) << left << "Separate file logging enabled: " << setup.logDataSeparately << endl;
  cout << setfill(s) << setw(w) << left << "Socket communication enabled: " << setup.socketCommunication << endl;
  cout << setfill(s) << setw(w) << left << "IP: " << setup.ip << endl;
  cout << setfill(s) << setw(w) << left << "Command port: " << setup.commandPort << endl;
  cout << setfill(s) << setw(w) << left << "Emg port: " << setup.emgPort << endl;
  cout << setfill(s) << setw(w) << left << "Accelerometer port: " << setup.accelerometerPort << endl;
  cout << setfill(s) << setw(w) << left << "Emg channels: " << setup.nEmgTot << endl;
  cout << setfill(s) << setw(w) << left << "Buffer rows: " << setup.bufferRows << endl;
  cout << setfill('-') << setw(w) << "-" << endl << endl;

  // set latch according to the number of active threads. The latch function manage the thread synchronization.
  // In particular it wait until all threads are ready, starting them at the same time
  latch.setCount(activeThreads);
  
  ////////
  // START THREADS ACCORDING TO THE CONFIGURATION
  ////////
//unique_ptr is a class to manage pointers. 
//A pointer is created for each object and for each threads
  unique_ptr<Myo> pMyo;
  unique_ptr<Cometa> pCometa;
  unique_ptr<ClientSocket> pClientSocket;
  unique_ptr<Manager> pManager;
  unique_ptr<LoggerToFile> pLoggerToFile;
  unique_ptr<LoggerToFile> pMyoLogger;
  unique_ptr<LoggerToFile> pCometaLogger;
  unique_ptr<LoggerToFile> pClientSocketLogger;
  unique_ptr<Socket> pEmgSocket;
  unique_ptr<Socket> pAccSocket;

  unique_ptr<thread> pMyoThread;
  unique_ptr<thread> pCometaThread;
  unique_ptr<thread> pClientSocketThread;
  unique_ptr<thread> pManagerThread;
  unique_ptr<thread> pLoggerToFileThread;
  unique_ptr<thread> pMyoLoggerThread;
  unique_ptr<thread> pCometaLoggerThread;
  unique_ptr<thread> pClientSocketLoggerThread;
  unique_ptr<thread> pEmgSocketThread;
  unique_ptr<thread> pAccSocketThread;
  
  //Objects are DINAMICALLY created in according to the configuration setup, assigning their address to the pointers created before.
  //After the creation, each object is moved into the reolative thread.
  //ALL IS MANAGED BY POINTERS
  pManager = unique_ptr<Manager>(new Manager(myoEmgQueue, myoAccQueue, cometaQueue, clientSocketQueue, emgQueue, accQueue, latch, setup));
  pManagerThread = unique_ptr<thread>(new thread(std::ref(*pManager)));

//If the device is active, the relative object and thread are created
  cout << "starting threads...\n";

  if (isMyoUsed) {
    pMyo = unique_ptr<Myo>(new Myo(myoEmgQueue, myoAccQueue, latch, setup.usedDevicesList.at(myoIndex)));
    pMyoThread = unique_ptr<thread>(new thread(std::ref(*pMyo)));
  }
  if (isCometaUsed) {
    pCometa = unique_ptr<Cometa>(new Cometa(cometaQueue, latch, setup.usedDevicesList.at(cometaIndex)));
    pCometaThread = unique_ptr<thread>(new thread(std::ref(*pCometa)));
  }
  if (isClientSocketUsed) {
    pClientSocket = unique_ptr<ClientSocket>(new ClientSocket(clientSocketQueue, latch, setup));
    pClientSocketThread = unique_ptr<thread>(new thread(std::ref(*pClientSocket)));
  }
  if (setup.logData) {
    pLoggerToFile = unique_ptr<LoggerToFile>(new LoggerToFile(emgQueue, latch, dataDir.string() + std::string("/log.txt")));
    pLoggerToFileThread = unique_ptr<thread>(new thread(std::ref(*pLoggerToFile)));
  }
  if (setup.logDataSeparately) {
    if (isMyoUsed) {
      pMyoLogger = unique_ptr<LoggerToFile>(new LoggerToFile(myoEmgQueue, latch, dataDir.string() + std::string("/myoLog.txt")));
      pMyoLoggerThread = unique_ptr<thread>(new thread(std::ref(*pMyoLogger)));
    }
    if (isCometaUsed) {
      pCometaLogger = unique_ptr<LoggerToFile>(new LoggerToFile(cometaQueue, latch, dataDir.string() + std::string("/cometaLog.txt")));
      pCometaLoggerThread = unique_ptr<thread>(new thread(std::ref(*pCometaLogger)));
    }
    if (isClientSocketUsed) {
      pClientSocketLogger = unique_ptr<LoggerToFile>(new LoggerToFile(clientSocketQueue, latch, dataDir.string() + std::string("/secondMyoLog.txt")));
      pClientSocketLoggerThread = unique_ptr<thread>(new thread(std::ref(*pClientSocketLogger)));
    }
  }
  if (setup.socketCommunication) {
    pEmgSocket = unique_ptr<Socket>(new Socket(emgQueue, latch, "emg", setup));
    pAccSocket = unique_ptr<Socket>(new Socket(accQueue, latch, "acc", setup));
    pEmgSocketThread = unique_ptr<thread>(new thread(std::ref(*pEmgSocket)));
    pAccSocketThread = unique_ptr<thread>(new thread(std::ref(*pAccSocket)));
  }

//The function join() returns when the thread execution has completed.
//After this, the thread become non-joinable and it can be destroyed safely.
  pManagerThread->join();
  if (isMyoUsed)
    pMyoThread->join();
  if (isCometaUsed)
    pCometaThread->join();
  if (isClientSocketUsed)
    pClientSocketThread->join();
  if (setup.logData)
    pLoggerToFileThread->join();
  if (setup.logDataSeparately){
    if (isMyoUsed)
      pMyoLoggerThread->join();
    if (isCometaUsed)
      pCometaLoggerThread->join();
    if (isClientSocketUsed)
      pClientSocketLoggerThread->join();
    }
  if (setup.socketCommunication) {
    pEmgSocketThread->join();
    pAccSocketThread->join();
  }
  

  /*
  if (setup.nUsedDevices == 1 && setup.usedDevicesList.at(0).deviceClass == "myo") {
    std::cout << "ONLY MYO" << std::endl;
    latch.setCount(activeThreads);

    Myo myo(myoEmgQueue, myoAccQueue, latch);
    Manager manager(myoEmgQueue, myoAccQueue, cometaQueue, emgQueue, accQueue, latch, setup);
    LoggerToFile loggerToFile(emgQueue, latch, dataDir.string() + std::string("/myoLogger.txt"));
    Socket emgSocket(emgQueue, latch, "emg");
    Socket accSocket(accQueue, latch, "acc");

    std::cout << "starting threads...\n";

    std::thread myoThread(std::ref(myo));
    std::thread managerThread(std::ref(manager));
    std::thread loggerToFileThread(std::ref(loggerToFile));
    std::thread emgSocketThread(std::ref(emgSocket));
    std::thread accSocketThread(std::ref(accSocket));

    myoThread.join();
    managerThread.join();
    loggerToFileThread.join();
    emgSocketThread.join();
    accSocketThread.join();
  }
  // Cometa Configuration
  else if (setup.nUsedDevices == 1 && setup.usedDevicesList.at(0).deviceClass == "cometa") {
    std::cout << "ONLY COMETA" << std::endl;
    latch.setCount(activeThreads);

    Cometa cometa(cometaQueue, latch);
    Manager manager(myoEmgQueue, myoAccQueue, cometaQueue, emgQueue, accQueue, latch, setup);
    LoggerToFile loggerToFile(emgQueue, latch, dataDir.string() + std::string("/cometaLog.txt"));
    Socket emgSocket(emgQueue, latch, "emg");
    Socket accSocket(accQueue, latch, "acc");

    std::cout << "starting threads...\n";

    std::thread cometaThread(std::ref(cometa));
    std::thread managerThread(std::ref(manager));
    std::thread loggerToFileThread(std::ref(loggerToFile));
    std::thread emgSocketThread(std::ref(emgSocket));
    std::thread accSocketThread(std::ref(accSocket));

    cometaThread.join();
    managerThread.join();
    loggerToFileThread.join();
    emgSocketThread.join();
    accSocketThread.join();
  }
  // Myo + Cometa Configuration
  if (setup.nUsedDevices == 2) {
    std::cout << "MYO + COMETA" << std::endl;
    latch.setCount(activeThreads);

    Myo myo(myoEmgQueue, myoAccQueue, latch);
    Cometa cometa(cometaQueue, latch);
    Manager manager(myoEmgQueue, myoAccQueue, cometaQueue, emgQueue, accQueue, latch, setup);
    LoggerToFile loggerToFile(emgQueue, latch, dataDir.string() + std::string("/cometaAndMyoLog.txt"));
    LoggerToFile myoLogger(myoEmgQueue, latch, dataDir.string() + std::string("/myoLog.txt"));
    LoggerToFile cometaLogger(cometaQueue, latch, dataDir.string() + std::string("/cometaLog.txt"));
    Socket emgSocket(emgQueue, latch, "emg");
    Socket accSocket(accQueue, latch, "acc");

    std::cout << "starting threads...\n";

    std::thread myoThread(std::ref(myo));
    std::thread cometaThread(std::ref(cometa));
    std::thread managerThread(std::ref(manager));
    std::thread loggerToFileThread(std::ref(loggerToFile));
    std::thread myoLoggerThread(std::ref(myoLogger));
    std::thread cometaLoggerThread(std::ref(cometaLogger));
    std::thread emgSocketThread(std::ref(emgSocket));
    std::thread accSocketThread(std::ref(accSocket));

    myoThread.join();
    cometaThread.join();
    managerThread.join();
    loggerToFileThread.join();
    myoLoggerThread.join();
    cometaLoggerThread.join();
    emgSocketThread.join();
    accSocketThread.join();
  }
  else {
    std::cout << "configuration not Accepted.\nExiting...\n";
    return 1;
  }*/
}
