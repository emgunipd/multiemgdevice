#ifndef Queues_h
#define Queues_h

#include "ceinms/Concurrency/Queue.h"
#include "MyoFrame.h"
#include "CometaFrame.h"
#include "DataFrame.h"

#include <vector>

extern CEINMS::Concurrency::Queue<MyoFrame> myoEmgQueue;
extern CEINMS::Concurrency::Queue<MyoFrame> myoAccQueue;
extern CEINMS::Concurrency::Queue<CometaFrame> cometaQueue;
extern CEINMS::Concurrency::Queue<CometaFrame> clientSocketQueue;
extern CEINMS::Concurrency::Queue<DataFrame> emgQueue;
extern CEINMS::Concurrency::Queue<DataFrame> accQueue;


#endif
